﻿using eclips.stat.data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;

namespace CaloriesCalculator.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [Authorize]
        public ActionResult Index()
        {
            var dp = new DataProvider();
            var players = dp.GetStatData();

            ViewData["Message"] = "Welcome to ASP.NET MVC!";
            var columns = new[] {
                new DataColumn("First Name", typeof(string)),
                new DataColumn("Nick Name", typeof(string)),
                new DataColumn("Middle Name", typeof(string)),
                new DataColumn("Last Name", typeof(string)),
                new DataColumn("Number of games", typeof(string))
            };
            DataTable dt = new DataTable("MyTable");
            foreach (var c in columns)
            {
                dt.Columns.Add(c);
            }

            foreach (var p in players)
            {
                DataRow row = dt.NewRow();
                row[columns[0].ColumnName] = p.Name;
                row[columns[1].ColumnName] = p.NickName;
                row[columns[2].ColumnName] = p.MiddleName;
                row[columns[3].ColumnName] = p.LastName;
                row[columns[4].ColumnName] = p.NumberOfGames;
                dt.Rows.Add(row);
            }

            return View(dt); //passing the DataTable as my Model
        }

        public void Login()
        {
            // Ensure there's a return URL
            if (Request.QueryString["ReturnUrl"] == null)
                Response.Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=" + Server.UrlEncode(FormsAuthentication.DefaultUrl));

            if (TempData.ContainsKey("allowLogin"))
            {
                // See if they've supplied credentials
                string authHeader = Request.Headers["Authorization"];
                if ((authHeader != null) && (authHeader.StartsWith("Basic")))
                {
                    // Parse username and password out of the HTTP headers
                    authHeader = authHeader.Substring("Basic".Length).Trim();
                    byte[] authHeaderBytes = Convert.FromBase64String(authHeader);
                    authHeader = Encoding.UTF7.GetString(authHeaderBytes);
                    string userName = authHeader.Split(':')[0];
                    string password = authHeader.Split(':')[1];

                    // Validate login attempt
                    if (FormsAuthentication.Authenticate(userName, password))
                    {
                        FormsAuthentication.RedirectFromLoginPage(userName, false);
                        return;
                    }
                }
            }

            // Force the browser to pop up the login prompt
            Response.StatusCode = 401;
            Response.AppendHeader("WWW-Authenticate", "Basic");
            TempData["allowLogin"] = true;

            // This gets shown if they click "Cancel" to the login prompt
            Response.Write("You must log in to access this URL.");
        }
    }
}