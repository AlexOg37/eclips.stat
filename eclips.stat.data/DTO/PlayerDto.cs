﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eclips.stat.data.DTO
{
    public class PlayerDto
    {
        public string Name { get; set; }
        public string NickName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string NumberOfGames { get; set; }
    }
}
