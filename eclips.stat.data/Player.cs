//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eclips.stat.data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Player
    {
        public Player()
        {
            this.Games = new HashSet<Game>();
        }
    
        public int Player_ID { get; set; }
        public string Player_FirstName { get; set; }
        public string Player_LastName { get; set; }
        public string Player_MiddleName { get; set; }
        public string Player_NickName { get; set; }
        public string Player_Sex { get; set; }
    
        public virtual ICollection<Game> Games { get; set; }
    }
}
