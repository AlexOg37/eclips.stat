﻿using eclips.stat.data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eclips.stat.data
{
    public class DataProvider
    {
        public List<PlayerDto> GetStatData()
        {
            using (var ec = new EclipseContext())
            {
                var res = new List<PlayerDto>();
                var pl = ec.Players.ToList();
                foreach (var p in pl)
                {
                    res.Add(new PlayerDto
                    {
                        Name = p.Player_FirstName,
                        NickName = p.Player_NickName,
                        MiddleName = p.Player_MiddleName,
                        LastName = p.Player_LastName,
                        NumberOfGames = p.Games.Count.ToString()
                    });
                }
                return res;
            }
        }
    }
}
